<?php if(!$noheader): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <style>body {font-family:Arial,Times;font-size:12px;color:#000;} h2 {margin-bottom:0px;padding:0;} ul {margin:0;padding:0;} table {width:100%;margin:0;padding:0;} p {margin:0;padding:0;} </style>
</head>
<body>
<?php endif; ?>
<?php $airline = $info->Airline; ?>
<?php
    if($passenger->field_passenger_status['und'][0]['value']=='INF' || $passenger->field_passenger_status['und'][0]['value']=='Inf') {
        $status = 'NS';
        $weight = '1PC/10KG';
    } else {
        $status = 'OK';
        $weight = $airline->field_baggage_allowance['und'][0]['value'];
    }
?>
<img src="<?php echo drupal_realpath($airline->field_airline_logo['und'][0]['uri']); ?>" height="60" style="position: absolute;margin-top: -20px;">
<?php echo $airline->field_airline_header['und'][0]['value']; ?>
<br><p style="text-align: center;">№ <?php echo $num; ?></p>
<table width="100%" border="1" cellspacing="0">
<tr>
    <td style="text-align:center;background: #CCC;">Вiд/до<br>From/To</td>
    <td style="text-align:center;background: #CCC;">Рейс No<br>Flight#</td>
    <td style="text-align:center;background: #CCC;">Клас<br>Class</td>
    <td style="text-align:center;background: #CCC;">Дата<br>Date</td>
    <td style="text-align:center;background: #CCC;">Час вильоту<br>Time</td>
    <td style="text-align:center;background: #CCC;">Статус<br>Status</td>
    <td style="text-align:center;background: #CCC;">Норма багажу<br>Baggage allow</td>
</tr>
<tr>
    <td><?php echo $info->AirportFromCode; ?> <?php echo $info->AirportFrom->name; ?></td>
    <td><?php echo $info->FlightNum; ?></td>
    <td>Y</td>
    <td><?php echo drupal_strtoupper($info->DatePDF); ?></td>
    <td><?php echo $info->TimeFrom; ?></td>
    <td><?php echo $status; ?></td>
    <td><?php echo $weight; ?></td>
</tr>
<?php
    if(isset($passenger->field_passenger_flight_back['und'][0]['target_id'])){
    $flightback = $passenger->field_passenger_flight_back['und'][0]['target_id'];
    $flightback = node_load($flightback);
    $infoback = tickets_flight_info($flightback);
?>
    <tr>
        <td><?php echo $info->AirportToCode; ?> <?php echo $info->AirportTo->name; ?></td>
        <td><?php echo $infoback->FlightNum; ?></td>
        <td>Y</td>
        <td><?php echo drupal_strtoupper($infoback->DatePDF); ?></td>
        <td><?php echo $infoback->TimeFrom; ?></td>
        <td><?php echo $status; ?></td>
        <td><?php echo $weight; ?></td>
    </tr>
    <tr>
        <td><?php echo $infoback->AirportToCode; ?> <?php echo $infoback->AirportTo->name; ?></td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
    </tr>
<?php } else { ?>
    <tr>
        <td><?php echo $info->AirportToCode; ?> <?php echo $info->AirportTo->name; ?></td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
    </tr>
    <tr>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
        <td>VOID</td>
    </tr>
<?php } ?>
</table>
<table width="100%" border="0" cellspacing="0">
    <tr>
        <td width="70%"><b>Name <?php echo $passenger->field_passenger_status['und'][0]['value']; ?> <?php echo $passenger->title; ?></b>__________________</td>
        <td width="7%" style="border-bottom:1px solid #000000;border-left:1px solid #000000;">bag.<br>pcs:</td>
        <td width="7%" style="border-bottom:1px solid #000000;border-left:1px solid #000000;">bag.<br>Weight</td>
        <td width="7%" style="border-bottom:1px solid #000000;border-left:1px solid #000000;">Exx.<br>Ess.</td>
        <td width="7%" style="border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;">bag<br>paid</td>
    </tr>
</table>
<br>
<?php echo $airline->field_airline_footer['und'][0]['value']; ?>
<?php if(!$noheader): ?>
</body></html>
<?php endif; ?>    