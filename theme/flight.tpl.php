<?php $info = tickets_flight_info($node); ?>
<div class="boarding-pass <?php echo $odd; ?><?php if(isset($order) && $order) echo ' order-pass'; ?>" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>">
  <header>
    <div class="flight">
      <small>АВИАКОМПАНИЯ</small>
      <strong><?php echo $info->Airline->name; ?></strong>
    </div>
  </header>
  <section class="cities">
    <div class="city">
      <small><?php echo $info->CountryFrom->name; ?>, <?php echo $info->CityFrom; ?></small>
      <strong><?php echo $info->AirportFromCode; ?></strong>
    </div>
    <div class="city">
      <small><?php echo $info->CountryTo->name; ?>, <?php echo $info->CityTo; ?></small>
      <strong><?php echo $info->AirportToCode; ?></strong>
    </div>
    <svg class="airplane">
      <use xlink:href="#airplane<?php echo $node->nid; ?>"></use>
    </svg>
    <div class="from-to" rel="<?php echo $info->Airline->tid; ?>/<?php echo $info->AirportFrom->tid; ?>-<?php echo $info->AirportTo->tid; ?>">
        <?php echo $info->AirportFrom->name; ?> <i class="fa fa-long-arrow-right"></i> <?php echo $info->AirportTo->name; ?>
    </div>
  </section>
  <section class="infos">
    <div class="places">
      <div class="box">
        <small>Терминал</small>
        <strong><?php echo $info->Terminal; ?></strong>
      </div>
      <div class="box">
        <small>Тип ВС</small>
        <strong><?php echo $info->BC; ?></strong>
      </div>
      <div class="box">
        <small>Емкость</small>
        <strong><?php echo $info->Places; ?></strong>
      </div>
      <div class="box">
        <small>Номер рейса</small>
        <strong><?php echo $info->FlightNum; ?></strong>
      </div>
    </div>
    <div class="times">
      <div class="box">
        <small>Время вылета</small>
        <strong><?php echo $info->TimeFrom; ?></strong>
      </div>
      <div class="box">
        <small>Время прилета</small>
        <strong><?php echo $info->TimeTo; ?></strong>
      </div>
      <?php /*
      <div class="box">
        <small>Длительность</small>
        <strong><?php echo $info->Duration; ?></strong>
      </div>
      */?>
      <div class="box">
        <small>Дата</small>
        <strong><?php echo $date; ?></strong>
      </div>
    </div>
  </section>
  <?php if(isset($order) && $order): ?>
    <div class="order-ticket">
        <a href="#" class="form-submit" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>">Бронировать</a>
    </div>
  <?php endif; ?>
</div>
<svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" display="none">
  <symbol  id="airplane<?php echo $node->nid; ?>" viewBox="243.5 245.183 25 21.633">
    <g>
      <path fill="#cb7731" d="M251.966,266.816h1.242l6.11-8.784l5.711,0.2c2.995-0.102,3.472-2.027,3.472-2.308
                              c0-0.281-0.63-2.184-3.472-2.157l-5.711,0.2l-6.11-8.785h-1.242l1.67,8.983l-6.535,0.229l-2.281-3.28h-0.561v3.566
                              c-0.437,0.257-0.738,0.724-0.757,1.266c-0.02,0.583,0.288,1.101,0.757,1.376v3.563h0.561l2.281-3.279l6.535,0.229L251.966,266.816z
                              "/>
    </g>
  </symbol>
</svg>